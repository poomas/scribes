{-# LANGUAGE TemplateHaskell #-}
module Scribe.Class where

import RIO

import Data.Aeson (ToJSON (..), Value)
import Katip


------------------------------------------------------------------------------------------
-- KatipScribe class
------------------------------------------------------------------------------------------
class (ToJSON a) => KatipScribe a where
  scribeName   :: a          -> Text
  scribeMake   :: PermitFunc -> Verbosity -> a -> IO (Text, Scribe)
  scribeToJson :: a          -> Value
  scribeToJson = toJSON

------------------------------------------------------------------------------------------
-- Existential KatipScribe
------------------------------------------------------------------------------------------
data AnyScribe = forall a. (Show a, Eq a, KatipScribe a) => AnyScribe a
instance Show   AnyScribe where show (AnyScribe a) = show a
instance Eq     AnyScribe where AnyScribe a == AnyScribe b = scribeName a == scribeName b
instance ToJSON AnyScribe where toJSON (AnyScribe a) = scribeToJson a
instance KatipScribe AnyScribe where
  scribeName     (AnyScribe a) = scribeName a
  scribeMake p v (AnyScribe a) = scribeMake p v a
  scribeToJson   (AnyScribe a) = scribeToJson a
