{-# LANGUAGE TemplateHaskell #-}
module Scribe.Katip where

import           RIO                        as R hiding (Builder)

import qualified Control.Monad.Logger       as L
import           Data.IP                    (fromSockAddr)
import           Data.String.Conv           (toS)
import           Data.Text.Lazy.Builder     (toLazyText)
import           Katip
import           Language.Haskell.TH.Syntax (Loc (..))
import           Lens.Micro.Platform        (makeLenses)
import           Network.HTTP.Types         (statusCode)
import           Network.Wai                (Middleware, Request (..), responseHeaders,
                                             responseStatus)
import           Network.Wai.Header         (contentLength)
import qualified RIO.ByteString             as B
import           RIO.List                   (find)
import qualified RIO.Map                    as M
import qualified RIO.Text                   as T
import qualified RIO.Text.Lazy              as TL
import           RIO.Time

import           Scribe.Class               (AnyScribe (..), KatipScribe (..))

-- | Record holding `Katip` log state. Custom because of instances
data KatipLS = KatipLS
  { _klsContext   :: !LogContexts
  , _klsNamespace :: !Namespace
  , _klsLogEnv    :: !LogEnv
  }
makeLenses ''KatipLS

------------------------------------------------------------------------------------------
-- Dynamic log level and selection change
------------------------------------------------------------------------------------------

-- | Map `Scribe` name with filtering options
type DynPermMap = Map Text DynamicPermitData
data DynamicPermitData = DynamicPermitData
  { dpdSeverity    :: Either Severity [Severity]
  , dpdMsgPrefixes :: [Either Text Text]
  , dpdNamespaces  :: [Either Text Text]
  , dpdPeriod      :: Maybe (UTCTime, UTCTime)
  , dpdHost        :: Maybe String
  , dpdModule      :: Maybe String
  } deriving (Show, Eq, Ord)

defDynamicPermitData :: Either Severity [Severity] -> DynamicPermitData
defDynamicPermitData initSeverity = DynamicPermitData
  { dpdSeverity    = initSeverity
  , dpdMsgPrefixes = []
  , dpdNamespaces  = []
  , dpdPeriod      = Nothing
  , dpdHost        = Nothing
  , dpdModule      = Nothing
  }

-- | Decides should an 'Item' be logged or not
dynamicPermit :: Text -> IORef DynPermMap -> Item a -> IO Bool
dynamicPermit sNm dpdRef Item{..} = M.lookup sNm <$> readIORef dpdRef <&> \case
  Nothing                    -> True
  Just DynamicPermitData{..} -> and
    [ checkSeverity                                          dpdSeverity
    , null   ns  || uniCheck (\xs -> (`elem` xs)) ns         dpdNamespaces
    , T.null msg || uniCheck (\x -> (`T.isPrefixOf` x)) msg  dpdMsgPrefixes
    , maybe True (\(s,e) -> s < _itemTime && _itemTime > e)  dpdPeriod
    , maybe True (_itemHost /=)                              dpdHost
    , maybe True (\x -> (loc_module <$> _itemLoc) == Just x) dpdModule
    -- , getEnvironment _itemEnv == "development"
    ]

  where
    msg = T.take 10 . TL.toStrict . toLazyText $ unLogStr _itemMessage
    ns  = unNamespace _itemNamespace

    checkSeverity (Left severity) = _itemSeverity >= severity
    checkSeverity (Right [])      = True
    checkSeverity (Right ss)      = _itemSeverity `elem` ss

    uniCheck f xs dpds = do
      let pos = rights dpds
          neg = lefts  dpds
      (     null pos ||      any (f xs) pos)  -- if null positive or it is in
        && (null neg || not (any (f xs) neg)) -- if null negative or it is NOT in


-- | Create 'Katip' `LogEnv` from a list of backends. If the list is empty, no logging
--   is performed.
mkScribes :: IORef DynPermMap -> Namespace -> Environment -> [AnyScribe] -> IO LogEnv
mkScribes ioDL ns re ks = do
  env <- initLogEnv ns re
  foldM reg env =<< mapM go ks
  where
    reg env (nm, scribe) = registerScribe nm scribe defaultScribeSettings env
    go (AnyScribe s)     = scribeMake (dynamicPermit (scribeName s) ioDL) V3 s


-- | Web request logger. For inspiration see ApacheLogger from wai-logger package.
katipReqLogger :: IORef KatipLS -> Middleware
katipReqLogger ioKatipLS app req sendResponse = do
  env <- view klsLogEnv <$> readIORef ioKatipLS
  app req $ \res -> do
    runKatipT env . logMsg "Request" InfoS . logStr $ B.intercalate " "
      [ fromMaybe "IP N/A" $ hrdXIP <|> sockIP <|> hdrHostIP, "- -" -- IP
      , requestMethod req                                           -- method
      , rawPathInfo req <> rawQueryString req                       -- full path
      , toS . show $ httpVersion req                                -- HTTP version
      , toS . show . statusCode $ responseStatus res                -- response code
      , toS . maybe "-" show . contentLength $ responseHeaders res  -- body length
      , inQuotes requestHeaderReferer                               -- referrer
      , inQuotes requestHeaderUserAgent                             -- user-agent
      ]
    sendResponse res

  where
    inQuotes f = "\"" <> maybe "N/A" toS (f req) <> "\""
    hdrHostIP  = requestHeaderHost req
    sockIP     = toS . show . fst <$> fromSockAddr (remoteHost req)
    hrdXIP     = snd <$> find ((`elem` ipHeaders) . fst) (requestHeaders req)
    ipHeaders  = ["x-real-ip", "x-forwarded-for"]



-- | Turn off logging only for the given block.
noLogging :: (Katip m) => m a -> m a
noLogging = localLogEnv clearScribes



--------------------------------------------------------------------------------
type KatipLogger  = forall m. (KatipContext m) => Text -> m ()
type KatipLoggerS = forall m. (KatipContext m) => Namespace -> Text -> m ()

-- | Logging of messages, automatically supplying payload and namespace
--   With *S, additional Namespace can be added
logDebugK :: KatipLogger
logDebugK = logLocM DebugS . ls
logDebugKS :: KatipLoggerS
logDebugKS ns = katipAddNamespace ns . logLocM DebugS . ls

logInfoK :: KatipLogger
logInfoK = logFM InfoS . ls
logInfoKS :: KatipLoggerS
logInfoKS ns = katipAddNamespace ns . logFM InfoS . ls

logNoticeK :: KatipLogger
logNoticeK = logFM NoticeS . ls
logNoticeKS :: KatipLoggerS
logNoticeKS ns = katipAddNamespace ns . logFM NoticeS . ls

logWarnK :: KatipLogger
logWarnK = logFM WarningS . ls
logWarnKS :: KatipLoggerS
logWarnKS ns = katipAddNamespace ns . logFM WarningS . ls

logErrorK :: KatipLogger
logErrorK = logFM ErrorS . ls
logErrorKS :: KatipLoggerS
logErrorKS ns = katipAddNamespace ns . logFM ErrorS . ls

logAlertK :: KatipLogger
logAlertK = logFM AlertS . ls
logAlertKS :: KatipLoggerS
logAlertKS ns = katipAddNamespace ns . logFM AlertS . ls


-- | Make `RIO`'s `LogFunc` to use `Katip` logging
rioToKatipLog :: KatipContext (RIO env) => env -> LogFunc
rioToKatipLog app = R.mkLogFunc $ \_cs src lvl msg ->
  runRIO app $ katipLog lvl src (R.utf8BuilderToText msg)
  where
    katipLog  R.LevelDebug    src = logDebugKS  (Namespace [src])
    katipLog  R.LevelInfo     src = logInfoKS   (Namespace [src])
    katipLog  R.LevelWarn     src = logWarnKS   (Namespace [src])
    katipLog  R.LevelError    src = logErrorKS  (Namespace [src])
    katipLog (R.LevelOther t) src = logNoticeKS (Namespace [src, t])


type ScribeMsg    = Text
type ScribeSource = Text
type ScribeLog    = ScribeSource -> L.LogLevel -> ScribeMsg -> IO ()

-- | Make `ScribeLog` to use `Katip` logging
rioToPoomasLog :: forall env. KatipContext (RIO env) => env -> ScribeLog
rioToPoomasLog app src lvl = runRIO app . katipLog lvl
  where
    katipLog :: L.LogLevel -> Text -> RIO env ()
    katipLog  L.LevelDebug    = logDebugKS  (Namespace [src])
    katipLog  L.LevelInfo     = logInfoKS   (Namespace [src])
    katipLog  L.LevelWarn     = logWarnKS   (Namespace [src])
    katipLog  L.LevelError    = logErrorKS  (Namespace [src])
    katipLog (L.LevelOther t) = logNoticeKS (Namespace [src, t])
