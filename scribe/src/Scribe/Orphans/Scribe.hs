{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Orphans.Scribe where

import RIO

import Data.Aeson          (Value)
import Data.String.Conv    (toS)
import Data.Swagger        (SwaggerType (..), ToParamSchema (..), ToSchema (..),
                            defaultSchemaOptions, description, enum_, example,
                            genericDeclareNamedSchema, schema, type_)
import Katip.Core          (Severity (..), renderSeverity, textToSeverity)
import Lens.Micro.Platform (mapped, (?~))
import Servant             (FromHttpApiData (..), MimeRender (..), PlainText,
                            ToHttpApiData (..))



------------------------------------------------------------------------------------------
-- Orphan instances for Severity
------------------------------------------------------------------------------------------
severityText :: [ Value ]
severityText = [ "Debug"
               , "Info"
               , "Warning"
               , "Error"
               , "Notice"
               , "Critical"
               , "Alert"
               , "Emergency"
               ]
instance MimeRender PlainText Severity where mimeRender _ = toS . renderSeverity
instance ToHttpApiData        Severity where toUrlPiece   = renderSeverity
instance FromHttpApiData      Severity where
  parseQueryParam = maybe (Left "Invalid Severity value") Right . textToSeverity
instance ToParamSchema        Severity where
 toParamSchema _ = mempty
   & type_ .~ Just SwaggerString
   & enum_ ?~ severityText
instance ToSchema Severity where
  declareNamedSchema p = genericDeclareNamedSchema defaultSchemaOptions p
    & mapped.schema.enum_       ?~ severityText
    & mapped.schema.description ?~ "Severity defines which log messages to log"
    & mapped.schema.example     ?~ "Debug"
