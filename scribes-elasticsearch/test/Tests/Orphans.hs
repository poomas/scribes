{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tests.Orphans where

import           Prelude

import           Data.Char                      (isAlpha)
import qualified Data.Text                      as T
import           Database.V5.Bloodhound         (IndexName (..), ReplicaCount (..),
                                                 Server (..), ShardCount (..))
import           Katip.Scribes.ElasticSearch    (IndexShardingPolicy (..))

import           Test.QuickCheck
import           Test.QuickCheck.Instances.Time ()

import           Scribe.ConsoleFile             (ConsoleScribe (..), FileScribe (..))
import           Scribe.ElasticSearch           (ElasticSearchScribe (..))


-- | Generate non-empty Text values
instance Arbitrary T.Text where
    arbitrary = T.pack <$> arbitraryNoEmpty
    shrink xs = T.pack <$> shrink (T.unpack xs)

-- | Generate random valid port
arbitraryPort :: Gen Int
arbitraryPort = choose (80,65535)

arbitraryNoEmpty :: Arbitrary a => Gen [a]
arbitraryNoEmpty = getNonEmpty <$> arbitrary

arbitraryURL :: Gen T.Text
arbitraryURL = do
  name <- T.pack . filter isAlpha <$> arbitrary
  port <- arbitraryPort
  pure $ "http://" <> name <> ":" <> T.pack (show port)

instance Arbitrary IndexShardingPolicy where
  arbitrary = oneof [ pure NoIndexSharding
                    , pure MonthlyIndexSharding
                    , pure WeeklyIndexSharding
                    , pure DailyIndexSharding
                    , pure HourlyIndexSharding
                    , pure EveryMinuteIndexSharding
                    ]

instance Arbitrary ConsoleScribe where
  arbitrary = ConsoleScribe <$> arbitrary -- consoleScribeName
                            <*> arbitrary -- consoleHidden
                            <*> arbitrary -- concoleDecorators

instance Arbitrary FileScribe where
  arbitrary = FileScribe <$> arbitrary -- fileScribeName
                         <*> arbitrary -- fileScribePath

instance Arbitrary ElasticSearchScribe where
  arbitrary = ElasticSearchScribe
                 <$> arbitrary                        -- esScribeName
                 <*> do Server       <$> arbitraryURL -- esScribeServer
                 <*> do IndexName    <$> arbitrary    -- esScribeIndex
                 <*> arbitrary                        -- esScribeShardPolicy
                 <*> do ShardCount   <$> arbitrary    -- esScribeShardCnt
                 <*> do ReplicaCount <$> arbitrary    -- esScribeReplicaCnt
