module Tests.JsonSpec (spec) where

import RIO

import Data.Aeson                  (FromJSON, encode, parseJSON)
import Data.Aeson.Types            (parseEither)
import Data.String.Conv            (toS)
import Data.Typeable               (typeOf)
import Data.Yaml                   (decodeThrow)
import Database.V5.Bloodhound      as V5
import Katip.Scribes.ElasticSearch as KES

import Test.Hspec                  (Spec, describe, it, shouldBe)
import Test.Hspec.QuickCheck       (prop)
import Test.QuickCheck             hiding (Result, Success)

import Scribe.ConsoleFile          (ConsoleScribe (..), FileScribe (..))
import Scribe.ElasticSearch        (ElasticSearchScribe (..))
import Tests.Orphans               ()

spec :: Spec
spec = do
  describe "JSON to/from conversion" $ do
    propJSON (Proxy :: Proxy ConsoleScribe)
    propJSON (Proxy :: Proxy FileScribe)
    propJSON (Proxy :: Proxy ElasticSearchScribe)

  describe "YAML" $ do
    it "parse ElasticSearchScribe list" $ do
      let yml = "   - name:   es"                    <> "\n"
             <> "     type:   elasticsearch"         <> "\n"
             <> "     server: http://localhost:9200" <> "\n"
             <> "     index:  poomas-tracker-log"    <> "\n"
      decodeThrow yml `shouldBe` Just
        [ElasticSearchScribe { esScribeName        = "es"
                             , esScribeServer      = Server "http://localhost:9200"
                             , esScribeIndex       = IndexName "poomas-tracker-log"
                             , esScribeShardPolicy = WeeklyIndexSharding
                             , esScribeShardCnt    = ShardCount 1
                             , esScribeReplicaCnt  = ReplicaCount 0
                             }
        ]

propJSON :: forall a . (Arbitrary a, ToJSON a, FromJSON a, Show a, Eq a, Typeable a)
         => Proxy a -> Spec
propJSON _ = prop testName $ \(a :: a) ->
  let json = "with " <> toS (encode a)
   in counterexample json (parseEither parseJSON (toJSON a) === Right a)
  where
    testName = show ty <> " FromJSON/ToJSON checks"
    ty       = typeOf (undefined :: a)
