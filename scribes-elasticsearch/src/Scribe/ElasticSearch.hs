{-# LANGUAGE TemplateHaskell #-}
module Scribe.ElasticSearch where

import RIO

import Data.Aeson                  (FromJSON (..), object, withObject, (.!=),
                                    (.:), (.:?), (.=))
import Database.V5.Bloodhound      as V5
import Katip.Scribes.ElasticSearch as KES
import Network.HTTP.Client         (defaultManagerSettings, newManager)


import Scribe.Class                (KatipScribe (..))

------------------------------------------------------------------------------------------
-- ElasticSearch scribe
------------------------------------------------------------------------------------------
data ElasticSearchScribe = ElasticSearchScribe
  { esScribeName        :: Text
  , esScribeServer      :: Server
  , esScribeIndex       :: IndexName
  , esScribeShardPolicy :: IndexShardingPolicy
  , esScribeShardCnt    :: ShardCount
  , esScribeReplicaCnt  :: ReplicaCount
  } deriving (Show, Generic)
instance Eq       ElasticSearchScribe where
  es1 == es2 = do esScribeName        es1 ==   esScribeName        es2
               && esScribeServer      es1 ==   esScribeServer      es2
               && esScribeIndex       es1 ==   esScribeIndex       es2
               && esScribeShardPolicy es1 `eq` esScribeShardPolicy es2
               && esScribeShardCnt    es1 ==   esScribeShardCnt    es2
               && esScribeReplicaCnt  es1 ==   esScribeReplicaCnt  es2
    where
      eq sp1 sp2 = case (sp1,sp2) of
        (NoIndexSharding,          NoIndexSharding)          -> True
        (MonthlyIndexSharding,     MonthlyIndexSharding)     -> True
        (WeeklyIndexSharding,      WeeklyIndexSharding)      -> True
        (DailyIndexSharding,       DailyIndexSharding)       -> True
        (HourlyIndexSharding,      HourlyIndexSharding)      -> True
        (EveryMinuteIndexSharding, EveryMinuteIndexSharding) -> True
        (CustomIndexSharding _,    CustomIndexSharding _)    -> True
        (_,                        _)                        -> False


instance FromJSON ElasticSearchScribe where
  parseJSON = withObject "ElasticSearchScribe" $ \o -> do
    o .: "type" >>= \case
      Just ("elasticsearch" :: String) -> do
        ElasticSearchScribe
          <$>                                           o .:  "name"
          <*>                                           o .:  "server"
          <*>                                           o .:  "index"
          <*> do maybe mzero pure =<< toShardPolicy <$> o .:? "shardPolicy" .!= "week"
          <*> do ShardCount   <$>                       o .:? "shardCnt"    .!= 1
          <*> do ReplicaCount <$>                       o .:? "replicaCnt"  .!= 0
      _                                -> mzero


instance ToJSON ElasticSearchScribe where
  toJSON (ElasticSearchScribe nm
                             (Server srv)
                             (IndexName idx)
                              sp
                             (ShardCount shard)
                             (ReplicaCount replica)
         ) =
    object [ "name"        .= nm
           , "type"        .= ("elasticsearch" :: String)
           , "server"      .= srv
           , "index"       .= idx
           , "shardPolicy" .= fromShardPolicy sp
           , "shardCnt"    .= shard
           , "replicaCnt"  .= replica
           ]
instance KatipScribe ElasticSearchScribe where
  scribeName = esScribeName
  scribeMake permitFunc vrb (ElasticSearchScribe nm srv idx sp shrd rplc) = do
    bhe <- mkBHEnv srv <$> newManager defaultManagerSettings
    let sett :: EsScribeCfg ESV5
        sett = defaultEsScribeCfgV5
               { essIndexSettings = IndexSettings shrd rplc
               , essIndexSharding = sp
               }
    (nm,) <$> mkEsScribe sett bhe idx (MappingName "doc") permitFunc vrb


-- | Parse SharedPolicy from String
toShardPolicy :: String -> Maybe IndexShardingPolicy
toShardPolicy = \case
  "none"   -> Just NoIndexSharding
  "month"  -> Just MonthlyIndexSharding
  "week"   -> Just WeeklyIndexSharding
  "day"    -> Just DailyIndexSharding
  "hour"   -> Just HourlyIndexSharding
  "minute" -> Just EveryMinuteIndexSharding
  _        -> Nothing

-- | Convert SharedPolicy to String
fromShardPolicy :: IndexShardingPolicy -> String
fromShardPolicy = \case
   NoIndexSharding          -> "none"
   MonthlyIndexSharding     -> "month"
   WeeklyIndexSharding      -> "week"
   DailyIndexSharding       -> "day"
   HourlyIndexSharding      -> "hour"
   EveryMinuteIndexSharding -> "minute"
   CustomIndexSharding _    -> "error: CustomIndexSharding"
