{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Orphans.ElasticSearch where

import           RIO

import           Data.Swagger           (NamedSchema (..), SwaggerType (..),
                                         ToSchema (..), description, type_)
import qualified Database.V5.Bloodhound as V5
import           Lens.Micro.Platform    ((?~))


------------------------------------------------------------------------------------------
-- Orphan instances for Katip
------------------------------------------------------------------------------------------

instance ToSchema V5.Server where
  declareNamedSchema _ = pure . NamedSchema (Just "Server") $ mempty
    & type_       .~ Just SwaggerString
    & description ?~ "Name of ElasticSearch server"
instance ToSchema V5.IndexName where
  declareNamedSchema _ = pure . NamedSchema (Just "IndexName") $ mempty
    & type_       .~ Just SwaggerString
    & description ?~ "Name of ElasticSearch index"
