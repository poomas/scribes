{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.EsConsoleFile where

import RIO

import Data.Aeson           (FromJSON (..))

import Scribe.Class         (AnyScribe (..))
import Scribe.ConsoleFile   (ConsoleScribe (..), FileScribe (..))
import Scribe.ElasticSearch (ElasticSearchScribe (..))


instance FromJSON AnyScribe where
  parseJSON o = maybe mzero pure
      =<< ((Just . AnyScribe <$> parseJSON @ElasticSearchScribe o)
      <|>  (Just . AnyScribe <$> parseJSON @ConsoleScribe       o)
      <|>  (Just . AnyScribe <$> parseJSON @FileScribe          o))
