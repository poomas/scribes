{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.ElasticSearch where

import RIO

import Data.Aeson           (FromJSON (..))

import Scribe.Class         (AnyScribe (..))
import Scribe.ElasticSearch (ElasticSearchScribe (..))

instance FromJSON AnyScribe where
  parseJSON o = maybe mzero pure
    =<< (Just . AnyScribe <$> parseJSON @ElasticSearchScribe o)
