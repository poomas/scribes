module Scribe.Poomas where

import RIO

import Control.Retry          (RetryPolicyM (..), exponentialBackoff, limitRetries,
                               retrying)
import Data.Aeson             (FromJSON (..), ToJSON (..), Value, encode, object,
                               withObject, (.:), (.=))
import Data.String.Conv       (toS)
import Data.Text.Lazy.Builder (toLazyText)
import Katip                  (Item (..), LogItem (..), LogStr (..), PermitFunc,
                               Scribe (..), Verbosity (..), getThreadIdText,
                               payloadObject)
import Katip.Core             (LocJs (..), ProcessIDJs (..))


import Scribe.Class           (KatipScribe (..))




------------------------------------------------------------------------------------------
-- Poomas scribe definition
------------------------------------------------------------------------------------------
type PoomasLogger = Text -> Text -> IO Int


data PoomasScribe = PoomasScribe
  { poomasScribeName :: Text
  , poomasRetry      :: RetryPolicyM IO  -- fixmee: make it configurable to some degree
  , poomasLogger     :: Maybe PoomasLogger
  }
  deriving (Generic)
instance Show     PoomasScribe where show PoomasScribe{..} = toS poomasScribeName
instance Eq       PoomasScribe where a == b = poomasScribeName a == poomasScribeName b
instance FromJSON PoomasScribe where
  parseJSON = withObject "PoomasScribe" $ \o -> do
    name <- o .: "name"
    o .: "type" >>= \case
      Just ("poomas" :: String) -> pure $ PoomasScribe name defPoomasRetry Nothing
      _                         -> mzero
instance ToJSON PoomasScribe where
  toJSON (PoomasScribe nm _ _ ) = object [ "name" .= nm
                                         , "type" .= ("poomas" :: String)
                                         ]
instance KatipScribe PoomasScribe where
  scribeName = poomasScribeName
  scribeMake permitFunc vrb p = (poomasScribeName p,) <$> mkPoomasScribe p permitFunc vrb


defPoomasRetry :: RetryPolicyM IO
defPoomasRetry = exponentialBackoff 25000 <> limitRetries 5

defPoomasScribe :: Text -> Maybe PoomasLogger -> PoomasScribe
defPoomasScribe nm lgr = PoomasScribe
  { poomasScribeName = nm
  , poomasRetry      = defPoomasRetry
  , poomasLogger     = lgr
  }

-------------------------------------------------------------------------------
mkPoomasScribe :: PoomasScribe -> PermitFunc -> Verbosity -> IO Scribe
mkPoomasScribe PoomasScribe{..} permit verb = pure $ Scribe
  { liPush           = sendToMonitor poomasLogger poomasRetry verb
  , scribeFinalizer  = pure ()
  , scribePermitItem = permit
  }


sendToMonitor :: LogItem a
              => Maybe PoomasLogger -> RetryPolicyM IO -> Verbosity -> Item a -> IO ()
sendToMonitor Nothing _ _ _  = pure ()
sendToMonitor (Just logger) retryPlcy verb item =
  void $ retrying retryPlcy (const pure)
    $ \_ -> tryAny (logger "origin" . toS . encode $ toValue verb item) <&> \case
              Right n | n > 0 -> False
              _               -> True

toValue :: LogItem a => Verbosity -> Item a -> Value
toValue verb Item{..} = object
  [ "app"    .= _itemApp
  , "env"    .= _itemEnv
  , "sev"    .= _itemSeverity
  , "thread" .=  getThreadIdText _itemThread
  , "host"   .= _itemHost
  , "pid"    .=  ProcessIDJs _itemProcess
  , "data"   .=  payloadObject verb _itemPayload
  , "msg"    .=  toLazyText (unLogStr _itemMessage)
  , "at"     .= _itemTime
  , "ns"     .= _itemNamespace
  , "loc"    .=  (LocJs <$> _itemLoc)
  ]
