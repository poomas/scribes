{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.Poomas where

import RIO

import Data.Aeson    (FromJSON (..))

import Scribe.Class  (AnyScribe (..))

instance FromJSON AnyScribe where parseJSON _ = mzero
