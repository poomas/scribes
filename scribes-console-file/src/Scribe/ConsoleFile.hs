{-# LANGUAGE TemplateHaskell #-}
module Scribe.ConsoleFile where

import RIO
import RIO.List               (intersperse, uncons)
import RIO.Set                (member)


import Data.Aeson             (FromJSON (..), ToJSON (..), object, withObject, (.!=),
                               (.:), (.:?), (.=))
import Data.Text              (justifyLeft)
import Data.Text.Lazy.Builder (fromText)
import Katip                  (ColorStrategy (..), Item (..), ItemFormatter, LogItem (..),
                               LogStr (..), Namespace (..), getThreadIdText, mkFileScribe,
                               mkHandleScribeWithFormatter, renderSeverity)
import Katip.Core             (locationToString)
import Katip.Format.Time      (formatAsLogTime)
import Katip.Scribes.Handle   (brackets, colorBySeverity, getKeys)

import Scribe.Class           (KatipScribe (..))

------------------------------------------------------------------------------------------
-- Console scribe
------------------------------------------------------------------------------------------
data ConsoleScribe = ConsoleScribe
  { consoleScribeName :: Text
  , consoleHidden     :: Set Text      -- ^ list of elements not to show in log
  , consoleDecorators :: [(Text,Text)] -- ^ list of (host,colour) pairs
  }
  deriving (Show, Eq, Generic)
instance FromJSON ConsoleScribe where
  parseJSON = withObject "ConsoleScribe" $ \o ->
    o .: "type" >>= \case
      Just ("console" :: String) -> ConsoleScribe <$> o .:  "name"
                                                  <*> o .:? "hidden"     .!= mempty
                                                  <*> o .:? "decorators" .!= mempty
      _                          -> mzero
instance ToJSON ConsoleScribe where
  toJSON (ConsoleScribe nm hs ds) = object [ "name"       .= nm
                                           , "hidden"     .= hs
                                           , "decorators" .= ds
                                           , "type"       .= ("console" :: String)
                                           ]
instance KatipScribe ConsoleScribe where
  scribeName = consoleScribeName
  scribeMake permitFunc verb (ConsoleScribe nm hs ds) = (nm,)
    -- <$> mkHandleScribe ColorIfTerminal stdout permitFunc verb
    <$> mkHandleScribeWithFormatter (colourHostBracketFormat hs ds)
                                     ColorIfTerminal
                                     stdout
                                     permitFunc
                                     verb


colourHostBracketFormat :: LogItem a => Set Text -> [(Text,Text)] -> ItemFormatter a
colourHostBracketFormat hiddens decors withColor verb Item{..} = mconcat
  [ onShow "time"      . brackets . fromText $ formatAsLogTime _itemTime
  , onShow "severity"  . brackets . fromText $ showSeverity _itemSeverity
  , onShow "server"    . brackets . fromText . pad 12 $ colorByHost decors withColor srv
  , onShow "namespace" . brackets $ mkNameSpaces ns
  , onShow "host"      . brackets $ fromString _itemHost
  , onShow "pid"       . brackets $ "PID "      <> fromString (show _itemProcess)
  , onShow "thread"    . brackets $ "ThreadId " <> fromText (getThreadIdText _itemThread)
  , onShow "keys"      $ mconcat ks
  , onShow "src_loc"   $ maybe mempty (brackets . fromString . locationToString) _itemLoc
  , onShow "message"   $ fromText " " <> unLogStr _itemMessage
  ]

  where
    ks               = map brackets $ getKeys verb _itemPayload
    mkNameSpaces     = mconcat . map fromText . intersperse "."
    onShow fld c     = if fld `member` hiddens then "" else c
    pad n            = justifyLeft n ' '
    showSeverity sev = colorBySeverity withColor sev (pad 7 $ renderSeverity sev)
    (srv,ns)         = fromMaybe ("!!No APP name!!",[])
                     . uncons
                     $ unNamespace _itemNamespace

colorByHost :: [(Text,Text)] -> Bool -> Text -> Text
colorByHost hostColors withColor host = bool host addColor withColor
  where
    addColor     = maybe host (colorize host) $ lookup host hostColors
    colorize s c = "\ESC["<> c <> "m" <> s <> "\ESC[0m"


------------------------------------------------------------------------------------------
-- File scribe
------------------------------------------------------------------------------------------
data FileScribe = FileScribe
  { fileScribeName  :: Text
  , fileScribePath  :: FilePath
  } deriving (Show, Eq, Generic)
instance FromJSON FileScribe where
  parseJSON = withObject "FileScribe" $ \o -> do
    o .: "type" >>= \case
      Just ("file" :: String) -> FileScribe <$> o .: "name"
                                            <*> o .: "path"
      _                       -> mzero
instance ToJSON FileScribe where
  toJSON (FileScribe nm path) = object [ "name" .= nm
                                       , "type" .= ("file" :: String)
                                       , "path" .= path
                                       ]
instance KatipScribe FileScribe where
  scribeName = fileScribeName
  scribeMake permitFunc vrb (FileScribe nm fNm) = (nm,) <$> mkFileScribe fNm permitFunc vrb
