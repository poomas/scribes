{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.ConsoleFile where

import RIO

import Data.Aeson         (FromJSON (..))

import Scribe.ConsoleFile (ConsoleScribe (..), FileScribe (..))
import Scribe.Class       (AnyScribe (..))

instance FromJSON AnyScribe where
  parseJSON o = maybe mzero pure
    =<< (Just . AnyScribe <$> parseJSON @ConsoleScribe o)
    <|> (Just . AnyScribe <$> parseJSON @FileScribe    o)
