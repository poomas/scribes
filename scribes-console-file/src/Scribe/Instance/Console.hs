{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.Console where

import RIO

import Data.Aeson         (FromJSON (..))

import Scribe.ConsoleFile (ConsoleScribe (..))
import Scribe.Class       (AnyScribe (..))

instance FromJSON AnyScribe where
  parseJSON o = maybe mzero pure =<< (Just . AnyScribe <$> parseJSON @ConsoleScribe o)
