{-# OPTIONS_GHC -fno-warn-orphans #-}
module Scribe.Instance.File where

import RIO

import Data.Aeson         (FromJSON (..))

import Scribe.ConsoleFile (FileScribe (..))
import Scribe.Class       (AnyScribe (..))

instance FromJSON AnyScribe where
  parseJSON o = maybe mzero pure =<< (Just . AnyScribe <$> parseJSON @FileScribe o)
