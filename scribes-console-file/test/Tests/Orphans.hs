{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tests.Orphans where

import           Prelude

import qualified Data.Text                      as T

import           Test.QuickCheck
import           Test.QuickCheck.Instances.Time ()

import           Scribe.ConsoleFile             (ConsoleScribe (..), FileScribe (..))


-- | Generate non-empty Text values
instance Arbitrary T.Text where
    arbitrary = T.pack <$> arbitraryNoEmpty
    shrink xs = T.pack <$> shrink (T.unpack xs)

arbitraryNoEmpty :: Arbitrary a => Gen [a]
arbitraryNoEmpty = getNonEmpty <$> arbitrary

instance Arbitrary ConsoleScribe where
  arbitrary = ConsoleScribe <$> arbitrary -- consoleScribeName
                            <*> arbitrary -- consoleHidden
                            <*> arbitrary -- concoleDecorators

instance Arbitrary FileScribe where
  arbitrary = FileScribe <$> arbitrary -- fileScribeName
                         <*> arbitrary -- fileScribePath
