module Tests.JsonSpec (spec) where

import RIO

import Data.Aeson                  (FromJSON, ToJSON, encode, parseJSON, toJSON)
import Data.Aeson.Types            (parseEither)
import Data.String.Conv            (toS)
import Data.Typeable               (typeOf)
import Data.Yaml                   (decodeThrow)

import Test.Hspec                  (Spec, describe, shouldBe, it)
import Test.Hspec.QuickCheck       (prop)
import Test.QuickCheck             hiding (Result, Success)

-- import Scribe.Class
import Scribe.ConsoleFile          (ConsoleScribe (..), FileScribe (..))
import Scribe.Instance.ConsoleFile ()
import Tests.Orphans               ()


spec :: Spec
spec = do
  describe "JSON to/from conversion" $ do
    propJSON (Proxy :: Proxy ConsoleScribe)
    propJSON (Proxy :: Proxy FileScribe)

  describe "YAML" $ do
    it "parse ConsoleScribe list" $ do
      let yml = "   - name:   terminal"                       <> "\n"
             <> "     type:   console"                        <> "\n"
             <> "     decorators:"                            <> "\n"
             <> "       - [\"Tracker\",\"32;44\"]"            <> "\n"
             <> "       - [\"Poomas-Hub-Beacon\",\"33;107\"]" <> "\n"
      decodeThrow yml `shouldBe` Just
        [ConsoleScribe { consoleScribeName = "terminal"
                       , consoleHidden     = mempty
                       , consoleDecorators = [ ( "Tracker","32;44")
                                             , ( "Poomas-Hub-Beacon","33;107")
                                             ]
                       }
        ]

    it "parse FileScribe list" $ do
      let yml = "   - name: data-logger"             <> "\n"
             <> "     type: file"                    <> "\n"
             <> "     path: logs/poomas-tracker.log" <> "\n"
      decodeThrow yml `shouldBe` Just
        [FileScribe { fileScribeName  = "data-logger"
                    , fileScribePath  = "logs/poomas-tracker.log"
                       }
        ]

propJSON :: forall a . (Arbitrary a, ToJSON a, FromJSON a, Show a, Eq a, Typeable a)
         => Proxy a -> Spec
propJSON _ = prop testName $ \(a :: a) ->
  let json = "with " <> toS (encode a)
   in counterexample json (parseEither parseJSON (toJSON a) === Right a)
  where
    testName = show ty <> " FromJSON/ToJSON checks"
    ty       = typeOf (undefined :: a)
