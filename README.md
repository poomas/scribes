# scribes

Functions, instances and a class for generic manipulation with different scribes, based on [Katip logging framework](https://hackage.haskell.org/package/katip).

It provides easy way to combine various Scribes with [poomas](https://bitbucket.org/vlatkoB/poomas).

To define a new scribe, 3 things have to be done:

1. Define a data type for the new Scribe settings
2. Define FromJSON, ToJSON and [KatipScribe](https://bitbucket.org/vlatkoB/scribes/src/default/scribe/src/Scribe/Class.hs#lines-13) instances for the settings
3. Define FromJSON for the needed combination of scribes (AnyScribe existential)

Once this is done, new Scribe can be used in configuration of poomas service.

Currently 4 types od Scribes are defined:

1. Console with custom highlighting
2. File (no rotation)
3. ElasticSearch
4. Poomas centralized logging
